<?php

namespace Container4pOxD73;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getExperienceRepositoryService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Repository\ExperienceRepository' shared autowired service.
     *
     * @return \App\Repository\ExperienceRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Repository\\ExperienceRepository'] = new \App\Repository\ExperienceRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
