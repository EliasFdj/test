<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/interests.html.twig */
class __TwigTemplate_665d43c20c6812cc29e22a1220c7ce6f386cab834c48a4f5790676c36bee2f2a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"resume-section\" id=\"interests\">
                <div class=\"resume-section-content\">
                    <h2 class=\"mb-5\">Centres d'interêt</h2>
                    <p>";
        // line 4
        echo twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "interets", [], "any", false, false, false, 4);
        echo "</p>
                </div>
            </section>";
    }

    public function getTemplateName()
    {
        return "home/interests.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/interests.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/interests.html.twig");
    }
}
