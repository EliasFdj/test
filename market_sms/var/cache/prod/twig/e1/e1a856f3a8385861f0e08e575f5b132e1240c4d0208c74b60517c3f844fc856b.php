<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/index.html.twig */
class __TwigTemplate_5d24ff66567f98ea06fc2455c75486cb488acb936fb1a7305d746c2e10eb452e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "home/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Accueil";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    ";
        // line 7
        $this->loadTemplate("home/navbar.html.twig", "home/index.html.twig", 7)->display($context);
        // line 8
        echo "    <div class=\"container-fluid p-0\">
         ";
        // line 9
        $this->loadTemplate("home/about.html.twig", "home/index.html.twig", 9)->display($context);
        // line 10
        echo "         <hr class=\"m-0\" />
            ";
        // line 11
        $this->loadTemplate("home/experience.html.twig", "home/index.html.twig", 11)->display($context);
        // line 12
        echo "            <hr class=\"m-0\" />
            ";
        // line 13
        $this->loadTemplate("home/education.html.twig", "home/index.html.twig", 13)->display($context);
        // line 14
        echo "            <hr class=\"m-0\" />
            ";
        // line 15
        $this->loadTemplate("home/skills.html.twig", "home/index.html.twig", 15)->display($context);
        // line 16
        echo "            <hr class=\"m-0\" />
            ";
        // line 17
        $this->loadTemplate("home/interests.html.twig", "home/index.html.twig", 17)->display($context);
        // line 18
        echo "            <hr class=\"m-0\" />
            ";
        // line 19
        $this->loadTemplate("home/awards.html.twig", "home/index.html.twig", 19)->display($context);
        // line 20
        echo "    </div>

";
    }

    public function getTemplateName()
    {
        return "home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 20,  91 => 19,  88 => 18,  86 => 17,  83 => 16,  81 => 15,  78 => 14,  76 => 13,  73 => 12,  71 => 11,  68 => 10,  66 => 9,  63 => 8,  61 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/index.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/index.html.twig");
    }
}
