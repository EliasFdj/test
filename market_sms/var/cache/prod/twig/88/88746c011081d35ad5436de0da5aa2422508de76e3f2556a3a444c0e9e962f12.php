<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/education.html.twig */
class __TwigTemplate_13ba9e3e5e030fc257ec9116116dd8b00ffc285ff8c0c9bf25656cfe80814196 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"resume-section\" id=\"education\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Cursus</h2>
\t\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cursus"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 5
            echo "\t\t<div class=\"d-flex flex-column flex-md-row justify-content-between mb-5\">
\t\t\t<div class=\"flex-grow-1\">
\t\t\t\t<h3 class=\"mb-0\">";
            // line 7
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "nomUniversite", [], "any", false, false, false, 7), "html", null, true);
            echo "</h3>
\t\t\t\t<div class=\"subheading mb-3\">";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "filiere", [], "any", false, false, false, 8), "html", null, true);
            echo "</div>
\t\t\t</div>
\t\t\t<div class=\"flex-shrink-0\">
\t\t\t\t<span class=\"text-primary\">";
            // line 11
            echo twig_escape_filter($this->env, $this->extensions['Twig\Extra\Intl\IntlExtension']->formatDateTime($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "dateDebut", [], "any", false, false, false, 11), "short", "none"), "html", null, true);
            echo " - ";
            if (twig_get_attribute($this->env, $this->source, $context["i"], "dateFin", [], "any", false, false, false, 11)) {
                echo " ";
                echo twig_escape_filter($this->env, $this->extensions['Twig\Extra\Intl\IntlExtension']->formatDateTime($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "dateFin", [], "any", false, false, false, 11), "short", "none"), "html", null, true);
                echo " ";
            } else {
                echo " Aujourd'hui ";
            }
            echo "</span>
\t\t\t</div>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "\t</div>
</section>
";
    }

    public function getTemplateName()
    {
        return "home/education.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 15,  60 => 11,  54 => 8,  50 => 7,  46 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/education.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/education.html.twig");
    }
}
