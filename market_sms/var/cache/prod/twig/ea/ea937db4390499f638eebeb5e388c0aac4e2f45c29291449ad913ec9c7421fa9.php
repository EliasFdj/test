<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/about.html.twig */
class __TwigTemplate_fb5c7acb9f8508ad0a0843e1600e0e23e9a727bcccfc62924d1f3aa84f115bff extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"resume-section\" id=\"about\">
\t<div class=\"resume-section-content\">
\t\t<h1 class=\"mb-0\">
\t\t\t";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "nom", [], "any", false, false, false, 4), "html", null, true);
        echo "
\t\t\t<span class=\"text-primary\">";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "prenom", [], "any", false, false, false, 5), "html", null, true);
        echo "</span>
\t\t</h1>
\t\t<div class=\"subheading mb-5\">
\t\t\t";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "adresse", [], "any", false, false, false, 8), "html", null, true);
        echo " · ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "telephone", [], "any", false, false, false, 8), "html", null, true);
        echo " ·
\t\t\t<a href=\"mailto:";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "email", [], "any", false, false, false, 9), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "email", [], "any", false, false, false, 9), "html", null, true);
        echo "</a>
\t\t</div>
\t\t<p class=\"lead mb-5\">";
        // line 11
        echo twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "description", [], "any", false, false, false, 11);
        echo "</p>
\t\t<div class=\"social-icons\">
\t\t\t<a class=\"social-icon\" href=\"#!\">
\t\t\t\t<i class=\"fab fa-linkedin-in\"></i>
\t\t\t</a>
\t\t\t<a class=\"social-icon\" href=\"#!\">
\t\t\t\t<i class=\"fab fa-github\"></i>
\t\t\t</a>
\t\t\t<a class=\"social-icon\" href=\"#!\">
\t\t\t\t<i class=\"fab fa-twitter\"></i>
\t\t\t</a>
\t\t\t<a class=\"social-icon\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "facebook", [], "any", false, false, false, 22), "html", null, true);
        echo "\">
\t\t\t\t<i class=\"fab fa-facebook-f\"></i>
\t\t\t</a>
\t\t</div>
\t</div>
</section>
";
    }

    public function getTemplateName()
    {
        return "home/about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 22,  65 => 11,  58 => 9,  52 => 8,  46 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/about.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/about.html.twig");
    }
}
