<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/experience.html.twig */
class __TwigTemplate_4e7dd680cf4327187deb31c0fa05722e49e30d479d6b5e135c5d31ed01441fcc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"resume-section\" id=\"experience\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Experience professionelle</h2>
\t\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["experience"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 5
            echo "\t\t<div class=\"d-flex flex-column flex-md-row justify-content-between mb-5\">
\t\t\t<div class=\"flex-grow-1\">
\t\t\t\t<h3 class=\"mb-0\">";
            // line 7
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "role", [], "any", false, false, false, 7), "html", null, true);
            echo "</h3>
\t\t\t\t<div class=\"subheading mb-3\">";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "institution", [], "any", false, false, false, 8), "html", null, true);
            echo "</div>
\t\t\t\t<p>";
            // line 9
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "shortDescription", [], "any", false, false, false, 9), "html", null, true);
            echo "</p>
\t\t\t</div>
\t\t\t<div class=\"flex-shrink-0\">
\t\t\t\t<span class=\"text-primary\">";
            // line 12
            echo twig_escape_filter($this->env, $this->extensions['Twig\Extra\Intl\IntlExtension']->formatDateTime($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "dateDebut", [], "any", false, false, false, 12), "short", "none"), "html", null, true);
            echo " - ";
            if (twig_get_attribute($this->env, $this->source, $context["i"], "dateFin", [], "any", false, false, false, 12)) {
                echo " ";
                echo twig_escape_filter($this->env, $this->extensions['Twig\Extra\Intl\IntlExtension']->formatDateTime($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "dateFin", [], "any", false, false, false, 12), "short", "none"), "html", null, true);
                echo " ";
            } else {
                echo " Aujourd'hui ";
            }
            echo "</span>
\t\t\t</div>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "\t</div>
</section>
";
    }

    public function getTemplateName()
    {
        return "home/experience.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 16,  64 => 12,  58 => 9,  54 => 8,  50 => 7,  46 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/experience.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/experience.html.twig");
    }
}
