<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/awards.html.twig */
class __TwigTemplate_74360a235612f19ce4d1153b9b1cf81a11ade539634980f25941f74dd73f3a22 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"resume-section\" id=\"awards\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Diplômes</h2>
\t\t<ul class=\"fa-ul mb-0\">
\t\t";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["diplomes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 6
            echo "\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t";
            // line 10
            if (twig_get_attribute($this->env, $this->source, $context["i"], "classement", [], "any", false, false, false, 10)) {
                // line 11
                echo "\t\t\t\t";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "classement", [], "any", false, false, false, 11), "html", null, true);
                echo "
\t\t\t\t<sup>";
                // line 12
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["i"], "classement", [], "any", false, false, false, 12), 1))) {
                    echo "ère";
                } else {
                    echo "ème";
                }
                echo "</sup>
\t\t\t\tPlace - ";
            }
            // line 13
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "nom", [], "any", false, false, false, 13), "html", null, true);
            echo ": ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "etablissement", [], "any", false, false, false, 13), "html", null, true);
            echo " -";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "annee", [], "any", false, false, false, 13), "html", null, true);
            echo "
\t\t\t</li>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "\t\t</ul>
\t</div>
</section>
";
    }

    public function getTemplateName()
    {
        return "home/awards.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 16,  69 => 13,  60 => 12,  55 => 11,  53 => 10,  47 => 6,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home/awards.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/awards.html.twig");
    }
}
