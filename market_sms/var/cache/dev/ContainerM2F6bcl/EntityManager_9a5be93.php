<?php

namespace ContainerM2F6bcl;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHoldera337d = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer3bed7 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesf6ce2 = [
        
    ];

    public function getConnection()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getConnection', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getMetadataFactory', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getExpressionBuilder', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'beginTransaction', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getCache', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getCache();
    }

    public function transactional($func)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'transactional', array('func' => $func), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->transactional($func);
    }

    public function commit()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'commit', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->commit();
    }

    public function rollback()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'rollback', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getClassMetadata', array('className' => $className), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'createQuery', array('dql' => $dql), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'createNamedQuery', array('name' => $name), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'createQueryBuilder', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'flush', array('entity' => $entity), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'clear', array('entityName' => $entityName), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->clear($entityName);
    }

    public function close()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'close', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->close();
    }

    public function persist($entity)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'persist', array('entity' => $entity), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'remove', array('entity' => $entity), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'refresh', array('entity' => $entity), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'detach', array('entity' => $entity), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'merge', array('entity' => $entity), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getRepository', array('entityName' => $entityName), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'contains', array('entity' => $entity), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getEventManager', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getConfiguration', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'isOpen', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getUnitOfWork', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getProxyFactory', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'initializeObject', array('obj' => $obj), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'getFilters', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'isFiltersStateClean', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'hasFilters', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return $this->valueHoldera337d->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer3bed7 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHoldera337d) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHoldera337d = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHoldera337d->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, '__get', ['name' => $name], $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        if (isset(self::$publicPropertiesf6ce2[$name])) {
            return $this->valueHoldera337d->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera337d;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera337d;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera337d;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera337d;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, '__isset', array('name' => $name), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera337d;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldera337d;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, '__unset', array('name' => $name), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera337d;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldera337d;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, '__clone', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        $this->valueHoldera337d = clone $this->valueHoldera337d;
    }

    public function __sleep()
    {
        $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, '__sleep', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;

        return array('valueHoldera337d');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer3bed7 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer3bed7;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer3bed7 && ($this->initializer3bed7->__invoke($valueHoldera337d, $this, 'initializeProxy', array(), $this->initializer3bed7) || 1) && $this->valueHoldera337d = $valueHoldera337d;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldera337d;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHoldera337d;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
