<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/awards.html.twig */
class __TwigTemplate_a5d6efa7e66eef1819b943a2509b1b7c3a2e81fb9f939c84039ed3813d486ffa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/awards.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/awards.html.twig"));

        // line 1
        echo "<section class=\"resume-section\" id=\"awards\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Awards & Certifications</h2>
\t\t<ul class=\"fa-ul mb-0\">
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\tGoogle Analytics Certified Developer
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\tMobile Web Specialist - Google Certification
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t1
\t\t\t\t<sup>st</sup>
\t\t\t\tPlace - University of Colorado Boulder - Emerging Tech Competition 2009
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t1
\t\t\t\t<sup>st</sup>
\t\t\t\tPlace - University of Colorado Boulder - Adobe Creative Jam 2008 (UI Design Category)
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t2
\t\t\t\t<sup>nd</sup>
\t\t\t\tPlace - University of Colorado Boulder - Emerging Tech Competition 2008
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t1
\t\t\t\t<sup>st</sup>
\t\t\t\tPlace - James Buchanan High School - Hackathon 2006
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t3
\t\t\t\t<sup>rd</sup>
\t\t\t\tPlace - James Buchanan High School - Hackathon 2005
\t\t\t</li>
\t\t</ul>
\t</div>
</section>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/awards.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"resume-section\" id=\"awards\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Awards & Certifications</h2>
\t\t<ul class=\"fa-ul mb-0\">
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\tGoogle Analytics Certified Developer
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\tMobile Web Specialist - Google Certification
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t1
\t\t\t\t<sup>st</sup>
\t\t\t\tPlace - University of Colorado Boulder - Emerging Tech Competition 2009
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t1
\t\t\t\t<sup>st</sup>
\t\t\t\tPlace - University of Colorado Boulder - Adobe Creative Jam 2008 (UI Design Category)
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t2
\t\t\t\t<sup>nd</sup>
\t\t\t\tPlace - University of Colorado Boulder - Emerging Tech Competition 2008
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t1
\t\t\t\t<sup>st</sup>
\t\t\t\tPlace - James Buchanan High School - Hackathon 2006
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t3
\t\t\t\t<sup>rd</sup>
\t\t\t\tPlace - James Buchanan High School - Hackathon 2005
\t\t\t</li>
\t\t</ul>
\t</div>
</section>
", "home/awards.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/awards.html.twig");
    }
}
