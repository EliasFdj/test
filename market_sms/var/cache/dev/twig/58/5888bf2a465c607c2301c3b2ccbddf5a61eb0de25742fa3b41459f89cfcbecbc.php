<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/awards.html.twig */
class __TwigTemplate_01f3972c2e3e18c5553a1e111a87c237edf3262d39255c6e1a1f58b82e14bbec extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/awards.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/awards.html.twig"));

        // line 1
        echo "<section class=\"resume-section\" id=\"awards\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Diplômes</h2>
\t\t<ul class=\"fa-ul mb-0\">
\t\t";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["diplomes"]) || array_key_exists("diplomes", $context) ? $context["diplomes"] : (function () { throw new RuntimeError('Variable "diplomes" does not exist.', 5, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 6
            echo "\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t";
            // line 10
            if (twig_get_attribute($this->env, $this->source, $context["i"], "classement", [], "any", false, false, false, 10)) {
                // line 11
                echo "\t\t\t\t";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "classement", [], "any", false, false, false, 11), "html", null, true);
                echo "
\t\t\t\t<sup>";
                // line 12
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["i"], "classement", [], "any", false, false, false, 12), 1))) {
                    echo "ère";
                } else {
                    echo "ème";
                }
                echo "</sup>
\t\t\t\tPlace - ";
            }
            // line 13
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "nom", [], "any", false, false, false, 13), "html", null, true);
            echo ": ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "etablissement", [], "any", false, false, false, 13), "html", null, true);
            echo " -";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "annee", [], "any", false, false, false, 13), "html", null, true);
            echo "
\t\t\t</li>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "\t\t</ul>
\t</div>
</section>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/awards.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 16,  75 => 13,  66 => 12,  61 => 11,  59 => 10,  53 => 6,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"resume-section\" id=\"awards\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Diplômes</h2>
\t\t<ul class=\"fa-ul mb-0\">
\t\t{% for i in diplomes %}
\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-trophy text-warning\"></i>
\t\t\t\t</span>
\t\t\t\t{% if i.classement %}
\t\t\t\t{{i.classement}}
\t\t\t\t<sup>{% if i.classement == 1 %}ère{% else %}ème{% endif %}</sup>
\t\t\t\tPlace - {% endif %} {{i.nom}}: {{i.etablissement}} -{{i.annee}}
\t\t\t</li>
\t\t{% endfor %}
\t\t</ul>
\t</div>
</section>
", "home/awards.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/awards.html.twig");
    }
}
