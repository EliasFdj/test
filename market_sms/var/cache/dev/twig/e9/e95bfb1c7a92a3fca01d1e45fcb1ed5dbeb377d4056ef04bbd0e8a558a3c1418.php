<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/about.html.twig */
class __TwigTemplate_7ea96f9d3c9998dbbc26126db972159c22d461f993592e3fc3086ce158529dd0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/about.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/about.html.twig"));

        // line 1
        echo "<section class=\"resume-section\" id=\"about\">
\t<div class=\"resume-section-content\">
\t\t<h1 class=\"mb-0\">
\t\t\t";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 4, $this->source); })()), "nom", [], "any", false, false, false, 4), "html", null, true);
        echo "
\t\t\t<span class=\"text-primary\">";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 5, $this->source); })()), "prenom", [], "any", false, false, false, 5), "html", null, true);
        echo "</span>
\t\t</h1>
\t\t<div class=\"subheading mb-5\">
\t\t\t";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 8, $this->source); })()), "adresse", [], "any", false, false, false, 8), "html", null, true);
        echo " · ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 8, $this->source); })()), "telephone", [], "any", false, false, false, 8), "html", null, true);
        echo " ·
\t\t\t<a href=\"mailto:";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 9, $this->source); })()), "email", [], "any", false, false, false, 9), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 9, $this->source); })()), "email", [], "any", false, false, false, 9), "html", null, true);
        echo "</a>
\t\t</div>
\t\t<p class=\"lead mb-5\">";
        // line 11
        echo twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 11, $this->source); })()), "description", [], "any", false, false, false, 11);
        echo "</p>
\t\t<div class=\"social-icons\">
\t\t\t<a class=\"social-icon\" href=\"#!\">
\t\t\t\t<i class=\"fab fa-linkedin-in\"></i>
\t\t\t</a>
\t\t\t<a class=\"social-icon\" href=\"#!\">
\t\t\t\t<i class=\"fab fa-github\"></i>
\t\t\t</a>
\t\t\t<a class=\"social-icon\" href=\"#!\">
\t\t\t\t<i class=\"fab fa-twitter\"></i>
\t\t\t</a>
\t\t\t<a class=\"social-icon\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 22, $this->source); })()), "facebook", [], "any", false, false, false, 22), "html", null, true);
        echo "\">
\t\t\t\t<i class=\"fab fa-facebook-f\"></i>
\t\t\t</a>
\t\t</div>
\t</div>
</section>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 22,  71 => 11,  64 => 9,  58 => 8,  52 => 5,  48 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"resume-section\" id=\"about\">
\t<div class=\"resume-section-content\">
\t\t<h1 class=\"mb-0\">
\t\t\t{{user.nom}}
\t\t\t<span class=\"text-primary\">{{user.prenom}}</span>
\t\t</h1>
\t\t<div class=\"subheading mb-5\">
\t\t\t{{user.adresse}} · {{user.telephone}} ·
\t\t\t<a href=\"mailto:{{user.email}}\">{{user.email}}</a>
\t\t</div>
\t\t<p class=\"lead mb-5\">{{user.description|raw}}</p>
\t\t<div class=\"social-icons\">
\t\t\t<a class=\"social-icon\" href=\"#!\">
\t\t\t\t<i class=\"fab fa-linkedin-in\"></i>
\t\t\t</a>
\t\t\t<a class=\"social-icon\" href=\"#!\">
\t\t\t\t<i class=\"fab fa-github\"></i>
\t\t\t</a>
\t\t\t<a class=\"social-icon\" href=\"#!\">
\t\t\t\t<i class=\"fab fa-twitter\"></i>
\t\t\t</a>
\t\t\t<a class=\"social-icon\" href=\"{{user.facebook}}\">
\t\t\t\t<i class=\"fab fa-facebook-f\"></i>
\t\t\t</a>
\t\t</div>
\t</div>
</section>
", "home/about.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/about.html.twig");
    }
}
