<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/skills.html.twig */
class __TwigTemplate_07d101af5824d0e3ed4b60061539f4211795436bd55c19c86db50cf0fc05aac2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/skills.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/skills.html.twig"));

        // line 1
        echo "<section class=\"resume-section\" id=\"skills\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Skills</h2>
\t\t<div class=\"subheading mb-3\">Programming Languages & Tools</div>
\t\t<ul class=\"list-inline dev-icons\">
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-html5\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-css3-alt\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-js-square\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-angular\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-react\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-node-js\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-sass\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-less\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-wordpress\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-gulp\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-grunt\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-npm\"></i>
\t\t\t</li>
\t\t</ul>
\t\t<div class=\"subheading mb-3\">Workflow</div>
\t\t<ul class=\"fa-ul mb-0\">
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-check\"></i>
\t\t\t\t</span>
\t\t\t\tMobile-First, Responsive Design
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-check\"></i>
\t\t\t\t</span>
\t\t\t\tCross Browser Testing & Debugging
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-check\"></i>
\t\t\t\t</span>
\t\t\t\tCross Functional Teams
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-check\"></i>
\t\t\t\t</span>
\t\t\t\tAgile Development & Scrum
\t\t\t</li>
\t\t</ul>
\t</div>
</section>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/skills.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"resume-section\" id=\"skills\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Skills</h2>
\t\t<div class=\"subheading mb-3\">Programming Languages & Tools</div>
\t\t<ul class=\"list-inline dev-icons\">
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-html5\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-css3-alt\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-js-square\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-angular\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-react\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-node-js\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-sass\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-less\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-wordpress\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-gulp\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-grunt\"></i>
\t\t\t</li>
\t\t\t<li class=\"list-inline-item\">
\t\t\t\t<i class=\"fab fa-npm\"></i>
\t\t\t</li>
\t\t</ul>
\t\t<div class=\"subheading mb-3\">Workflow</div>
\t\t<ul class=\"fa-ul mb-0\">
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-check\"></i>
\t\t\t\t</span>
\t\t\t\tMobile-First, Responsive Design
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-check\"></i>
\t\t\t\t</span>
\t\t\t\tCross Browser Testing & Debugging
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-check\"></i>
\t\t\t\t</span>
\t\t\t\tCross Functional Teams
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-check\"></i>
\t\t\t\t</span>
\t\t\t\tAgile Development & Scrum
\t\t\t</li>
\t\t</ul>
\t</div>
</section>
", "home/skills.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/skills.html.twig");
    }
}
