<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/education.html.twig */
class __TwigTemplate_73b797057f96059bb9b2b38d5210c3cec6a78b8c03dcc232b2b6c458ced812eb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/education.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/education.html.twig"));

        // line 1
        echo "<section class=\"resume-section\" id=\"education\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Cursus</h2>
\t\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cursus"]) || array_key_exists("cursus", $context) ? $context["cursus"] : (function () { throw new RuntimeError('Variable "cursus" does not exist.', 4, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 5
            echo "\t\t<div class=\"d-flex flex-column flex-md-row justify-content-between mb-5\">
\t\t\t<div class=\"flex-grow-1\">
\t\t\t\t<h3 class=\"mb-0\">";
            // line 7
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "nomUniversite", [], "any", false, false, false, 7), "html", null, true);
            echo "</h3>
\t\t\t\t<div class=\"subheading mb-3\">";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "filiere", [], "any", false, false, false, 8), "html", null, true);
            echo "</div>
\t\t\t</div>
\t\t\t<div class=\"flex-shrink-0\">
\t\t\t\t<span class=\"text-primary\">";
            // line 11
            echo twig_escape_filter($this->env, $this->extensions['Twig\Extra\Intl\IntlExtension']->formatDateTime($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "dateDebut", [], "any", false, false, false, 11), "short", "none"), "html", null, true);
            echo " - ";
            if (twig_get_attribute($this->env, $this->source, $context["i"], "dateFin", [], "any", false, false, false, 11)) {
                echo " ";
                echo twig_escape_filter($this->env, $this->extensions['Twig\Extra\Intl\IntlExtension']->formatDateTime($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "dateFin", [], "any", false, false, false, 11), "short", "none"), "html", null, true);
                echo " ";
            } else {
                echo " Aujourd'hui ";
            }
            echo "</span>
\t\t\t</div>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "\t</div>
</section>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/education.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 15,  66 => 11,  60 => 8,  56 => 7,  52 => 5,  48 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"resume-section\" id=\"education\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Cursus</h2>
\t\t{% for i in cursus %}
\t\t<div class=\"d-flex flex-column flex-md-row justify-content-between mb-5\">
\t\t\t<div class=\"flex-grow-1\">
\t\t\t\t<h3 class=\"mb-0\">{{i.nomUniversite}}</h3>
\t\t\t\t<div class=\"subheading mb-3\">{{i.filiere}}</div>
\t\t\t</div>
\t\t\t<div class=\"flex-shrink-0\">
\t\t\t\t<span class=\"text-primary\">{{i.dateDebut|format_datetime('short', 'none')}} - {% if i.dateFin %} {{i.dateFin |format_datetime('short', 'none') }} {% else %} Aujourd'hui {% endif %}</span>
\t\t\t</div>
\t\t</div>
\t\t{% endfor %}
\t</div>
</section>
", "home/education.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/education.html.twig");
    }
}
