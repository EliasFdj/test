<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/skills.html.twig */
class __TwigTemplate_f6061d508d57ed717fac1c3817728b0de9a1afeb3eef03ed9dd35158905ee5a5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/skills.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/skills.html.twig"));

        // line 1
        echo "<section class=\"resume-section\" id=\"skills\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Compétences</h2>
\t\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categ"]) || array_key_exists("categ", $context) ? $context["categ"] : (function () { throw new RuntimeError('Variable "categ" does not exist.', 4, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 5
            echo "\t\t<div class=\"subheading mb-3\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["i"], "nom", [], "any", false, false, false, 5), "html", null, true);
            echo "</div>
\t\t<ul class=\"fa-ul mb-0\">
\t\t";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["i"], "competences", [], "any", false, false, false, 7));
            foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                // line 8
                echo "\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-check\"></i>
\t\t\t\t</span>
\t\t\t\t";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["j"], "nom", [], "any", false, false, false, 12), "html", null, true);
                echo "
\t\t\t</li>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 15
            echo "\t\t</ul>
\t\t<hr>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "\t</div>
</section>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/skills.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 18,  77 => 15,  68 => 12,  62 => 8,  58 => 7,  52 => 5,  48 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"resume-section\" id=\"skills\">
\t<div class=\"resume-section-content\">
\t\t<h2 class=\"mb-5\">Compétences</h2>
\t\t{% for i in categ %}
\t\t<div class=\"subheading mb-3\">{{i.nom}}</div>
\t\t<ul class=\"fa-ul mb-0\">
\t\t{% for j in i.competences %}
\t\t\t<li>
\t\t\t\t<span class=\"fa-li\">
\t\t\t\t\t<i class=\"fas fa-check\"></i>
\t\t\t\t</span>
\t\t\t\t{{j.nom}}
\t\t\t</li>
\t\t{% endfor %}
\t\t</ul>
\t\t<hr>
\t\t{% endfor %}
\t</div>
</section>
", "home/skills.html.twig", "/Users/eliasbeezneo/Desktop/SaraPortfolio/templates/home/skills.html.twig");
    }
}
