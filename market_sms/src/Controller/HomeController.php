<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Repository\CursusRepository;
use App\Repository\DiplomeRepository;
use App\Repository\CategorieRepository;
use App\Repository\ExperienceRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(UserRepository $repo,
                          CursusRepository $repocursus,
                          ExperienceRepository $repoexp,
                          CategorieRepository $repocateg,
                          DiplomeRepository $repodip): Response
    {
        return $this->render('home/index.html.twig', [
            'user' => $repo->getSara(),
            'cursus'=>$repocursus->findBy(
                [],
                ['id' => 'DESC']
              ),
              'categ'=>$repocateg->findAll(),

              'experience'=>$repoexp->findBy(
                [],
                ['id' => 'DESC']
              ),
              'diplomes'=>$repodip->findBy(
                [],
                ['annee' => 'DESC']
              ),   
        ]);
    }
}
