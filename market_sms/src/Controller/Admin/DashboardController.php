<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Cursus;
use App\Entity\Diplome;
use App\Entity\Categorie;
use App\Entity\Experience;
use App\Entity\Competences;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('SaraIzem.');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-address-card', User::class);
        yield MenuItem::linkToCrud('Compétences', 'fas fa-check-square', Competences::class);
        yield MenuItem::linkToCrud('Expérience', 'fas fa-briefcase', Experience::class);
        yield MenuItem::linkToCrud('Cursus', 'fas fa-school', Cursus::class);
        yield MenuItem::linkToCrud('Categorie', 'fas fa-cubes', Categorie::class);
        yield MenuItem::linkToCrud('Diplomes', 'fas fa-award', Diplome::class);
    }
}
