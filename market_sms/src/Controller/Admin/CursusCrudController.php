<?php

namespace App\Controller\Admin;

use App\Entity\Cursus;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CursusCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Cursus::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nomUniversite'),
            TextField::new('filiere'),
            DateTimeField::new('dateDebut')->setLabel('Date de début'),
            DateTimeField::new('dateFin')->setLabel('Date de fin'),
        ];
    }

    public function configureCrud(Crud $crud): crud
    {
        return $crud->setDefaultSort(['id'=> 'DESC']);
    }
    
}
