<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            
            TextField::new('nom'),
            TextField::new('prenom'),
            EmailField::new('email'),
            TextField::new('imageFile')->setFormType(VichImageType::class)->hideOnIndex(),
            ImageField::new('image')->setBasePath('/uploads/profile/')->onlyOnIndex(),
            TextField::new('password')->hideOnIndex(),
            ChoiceField::new('roles')->setChoices([
                
                'Sara'=>'ROLE_SARA',
                'Super-User'=>'ROLE_SUSER',
                'Administrateur'=> 'ROLE_ADMIN',
                'Utilisateur'=>'ROLE_USER',
                
            ])->allowMultipleChoices(),
            TextEditorField::new('description')->hideOnIndex(),
            TextEditorField::new('interets')->hideOnIndex(),  
            TextField::new('adresse'),
            TextField::new('telephone'),
            TextField::new('facebook')->hideOnIndex(),
            TextField::new('twitter')->hideOnIndex(),
        ];
    }
    
}
